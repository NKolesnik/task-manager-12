package ru.t1consulting.nkolesnik.tm.api.repository;

import ru.t1consulting.nkolesnik.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    Project add(Project project);

    Project create(String name);

    Project create(String name, String description);

    List<Project> findAll();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project remove(Project project);

    void clear();

    Project removeById(String id);

    Project removeByIndex(Integer index);

    int getSize();

}
