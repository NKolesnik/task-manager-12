package ru.t1consulting.nkolesnik.tm.api.service;

import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.model.Task;

import java.util.List;

public interface ITaskService {

    Task add(Task task);

    Task remove(Task task);

    Task create(String name);

    Task create(String name, String description);

    List<Task> findAll();

    void clear();

    Task findOneByIndex(Integer index);

    Task findOneById(String id);

    Task updateByIndex(Integer index, String name, String description);

    Task updateById(String id, String name, String description);

    Task removeByIndex(Integer index);

    Task removeById(String id);

    Task changeTaskStatusByIndex(Integer index, Status status);

    Task changeTaskStatusById(String id, Status status);

}
